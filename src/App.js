import "gestalt/dist/gestalt.css";
import React, { useEffect, useState, useRef } from "react";
import { Box, Spinner } from "gestalt";
import { Icon, Text, Button, Notification } from "atomize";
import { Dropdown, Anchor, Div, Row, Col } from "atomize";
import { Upload, Slider, Popover } from "antd";
import axios from "axios";
import "./App.css";
import "antd/dist/antd.css";

const ColorDefault = [
  "red",
  "green",
  "blue",
  "yellow",
  "orange",
  "pink",
  "cyan",
  "purple",
];

const Video = ({ url }) => {
  let videoRef = useRef();
  useEffect(() => {
    videoRef.current.src = url;
    console.log(videoRef);
    videoRef.current.play();
  }, [url]);
  return (
    <video ref={videoRef} width="100%" controls>
      <source src={url} type="video/mp4" />
    </video>
  );
};

function App() {
  let [searchColor, setSearchColor] = useState("");
  let [segData, setSegData] = useState({});
  let [videoSrc, setVideoSrc] = useState("");
  let [isLoading, setIsLoading] = useState(false);
  let [videoPath, setVideoPath] = useState("");

  let [uploading, setUploading] = useState(false);
  let [fileList, setFileList] = useState([]);
  let [showFileList, setShowFileList] = useState([]);
  let [showMess, setShowMess] = useState(false);
  let [frameSkip, setFrameSkip] = useState(10);
  let [colorArea, setColorArea] = useState(1200);
  let [dropDown, setDropDown] = useState(false);
  let [objClass, setObjClass] = useState("person");
  let [dataAndThumb, setDataAndThumb] = useState([]);
  const menuList = (
    <Div>
      {["person", "car", "bus", "truck"].map((name, index) => (
        <Anchor
          d="block"
          p={{ y: "0.25rem" }}
          onClick={() => {
            setObjClass(name);
            setDropDown(false);
          }}
        >
          {name}
        </Anchor>
      ))}
    </Div>
  );
  const content = (
    <Div>
      <Box display="flex" justifyContent="between">
        {/* <Text textSize="caption">Process Speed:</Text> */}
        <Slider
          defaultValue={1}
          min={1}
          max={12}
          step={1}
          onChange={(val) => setFrameSkip(val)}
          value={frameSkip}
        />
      </Box>
      <Box display="flex" justifyContent="between">
        {/* <Text textSize="caption">Color Area:</Text> */}
        <Slider
          defaultValue={100}
          min={100}
          max={1200}
          step={100}
          onChange={(val) => setColorArea(val)}
          value={colorArea}
        />
      </Box>
    </Div>
  );
  const handleUpload = async () => {
    function sendFile() {
      setUploading(true);
      const formData = new FormData();
      fileList.forEach((file) => {
        formData.append("file", file);
      });
      formData.append("frame_skip", frameSkip);
      formData.append("color_areas", colorArea);

      axios({
        method: "post",
        url: "/file_upload",
        data: formData,
        headers: { "Content-Type": "multipart/form-data" },
      })
        .then((response) => {
          setVideoPath(response.data.video_path);
          setUploading(false);
        })
        .catch((error) => {
          setUploading(false);
          console.log(error);
        });
    }
    await axios
      .post("/check_exist_file", {
        video_file_path: showFileList[0].name,
        frame_skip: frameSkip,
        color_areas: colorArea,
      })
      .then((response) => {
        if (response.data.message === "Video already exist") {
          if (
            window.confirm(
              "Video already exists on the system! Do you want to overwrite and reprocess them? "
            )
          ) {
            sendFile();
          } else {
            setVideoPath(response.data.video_path);
          }
        } else {
          sendFile();
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const props = {
    onRemove: (file) => {
      const index = fileList.indexOf(file);
      const newFileList = fileList.slice();
      newFileList.splice(index, 1);
      setFileList(newFileList);
    },
    beforeUpload: (file) => {
      setFileList([...fileList, file]);
      return false;
    },
    onChange: (file) => {
      let data = [...fileList, file.file];
      data = data.slice(-1);
      file.fileList = file.fileList.slice(-1);
      setShowFileList(file.fileList);
      setFileList(data);
    },
    fileList: showFileList,
  };

  const findColor = async (color) => {
    setIsLoading(true);
    setDataAndThumb([]);
    await axios
      .post("/search", {
        video_file_path: videoPath,
        color: color,
        frame_skip: frameSkip,
        color_areas: colorArea,
        obj_class_name: objClass,
      })
      .then((response) => {
        if (response.data.message === "No result found!") {
          setSegData([]);
          setIsLoading(false);
        } else {
          let tempData = [];
          let count = 0;
          Object.keys(response.data[2].data).forEach(async (e) => {
            let src = await getThumb(e);
            let temp = {
              key: e,
              data: response.data[2].data[e],
              thumb: URL.createObjectURL(src),
            };
            tempData.push(temp);
            count++;
            if (count === Object.keys(response.data[2].data).length) {
              setDataAndThumb(tempData);
            }
          });
          setSegData(response.data[2].data);
          setIsLoading(false);
        }
      })
      .catch((error) => {
        setIsLoading(false);
        console.log(error);
      });
  };

  const playSeg = (segTime, dataSeg) => {
    setIsLoading(true);
    axios({
      method: "post",
      url: "/play_seg",
      data: {
        video_file_path: videoPath,
        frame_skip: frameSkip,
        color_areas: colorArea,
        seg_time: segTime,
        data: dataSeg,
        color: searchColor,
      },
    })
      .then((response) => {
        setIsLoading(false);
        setVideoSrc(
          `/video?video_path=${response.data.search_result_video_URL}`
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getThumb = async (segTime) => {
    let result = await axios({
      method: "post",
      url: "/get_image_thumbnail",
      data: { video_file_path: videoPath, seg_time: segTime },
      responseType: "blob",
    }).catch((error) => {
      console.log(error);
    });
    console.log(result);
    return result.data;
  };

  useEffect(() => {
    setSegData([]);
  }, [videoPath]);
  return (
    <Box display="flex">
      <Notification
        bg="info700"
        isOpen={showMess}
        onClose={() => setShowMess(false)}
        prefix={
          <Icon name="Success" color="white" size="18px" m={{ r: "0.5rem" }} />
        }
      >
        file đã tồn tại trong hệ thống
      </Notification>

      <Box column={4}>
        <div className="main_side">
          <div className="top_video">
            <Video url={videoSrc} />
          </div>
          <div className="tool">
            <Box>
              {uploading ? (
                <Spinner show={true} accessibilityLabel="Example spinner" />
              ) : (
                <div>
                  <Div d="flex" align="center">
                    <Button h="2.2rem" maxW="100%">
                      {showFileList[0] && (
                        <div className="t_o">{showFileList[0].name}</div>
                      )}
                      <Div m={{ l: "0.5rem" }}>
                        <Upload
                          {...props}
                          onClick={(e) => {
                            e.stopPropagation();
                          }}
                        >
                          <Icon name="Folder" size="20px" color="white" />
                        </Upload>
                        /
                        <Icon
                          name="Upload"
                          size="20px"
                          color="white"
                          onClick={handleUpload}
                        />
                      </Div>
                    </Button>
                  </Div>
                </div>
              )}
              <Box>{content}</Box>
            </Box>
            <Div m={{ b: "1rem" }}>
              <Dropdown
                isOpen={dropDown}
                onClick={() => setDropDown(!dropDown)}
                menu={menuList}
                h="2rem"
              >
                {objClass}
              </Dropdown>
            </Div>
            <Box>
              <Box display="flex" justifyContent="between" alignItems="center">
                {ColorDefault.map((e, i) => (
                  <div
                    className="color_box"
                    style={{
                      backgroundColor: e,
                      border: `${searchColor === e ? "2px solid white" : ""}`,
                    }}
                    onClick={() => {
                      setSearchColor(e);
                      findColor(e);
                    }}
                    key={i}
                  ></div>
                ))}
              </Box>
            </Box>
          </div>
        </div>
      </Box>
      <Box column={8}>
        <div className="side_list">
          {!uploading && Object.keys(segData).length > 0 ? (
            <Box>
              <Text textColor="white" m={{ r: "1rem", t: "1rem" }}>
                RESULT
              </Text>

              <Row>
                {dataAndThumb.map((e, i) => (
                  <Col key={i} size={3}>
                    <div className="ratio">
                      <img src={e.thumb} />
                    </div>
                    <Box
                      display="flex"
                      justifyContent="between"
                      alignItems="center"
                      key={i}
                      paddingX={3}
                    >
                      <Box>
                        <Text
                          textColor="gray700"
                          textSize="body"
                          textWeight="600"
                        >
                          {e.key}
                        </Text>
                      </Box>
                      <Box>
                        <Button
                          prefix={
                            <Icon name="Play" size="16px" color="white" />
                          }
                          rounded="circle"
                          shadow="12"
                          hoverShadow="4"
                          bg="transparent"
                          h="2rem"
                          w="2rem"
                          onClick={() => playSeg(e.key, e.data)}
                        ></Button>
                      </Box>
                    </Box>
                  </Col>
                ))}
              </Row>
            </Box>
          ) : (
            <Text textColor="white" m={{ r: "1rem", t: "1rem" }}>
              NO RESULT
            </Text>
          )}
        </div>
      </Box>
    </Box>
  );
}

export default App;
